﻿
/**
 * Module dependencies.
 */

var express = require('express'),
    fs = require('fs');

var app = module.exports = express.createServer();
var port = 80;

// controllers
var controllers = fs.readdirSync(__dirname + '/controllers/');
for (var i = 0 ; i < controllers.length ; i++) {
    controllers[i] = controllers[i].substring(0, controllers[i].lastIndexOf('.'))
    global[controllers[i]] = require('./controllers/' + controllers[i]);
}


// Configuration

app.configure(function () {
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    app.use(express.session({ secret: 'your secret here' }));
    app.use(require('stylus').middleware({ src: __dirname + '/public' }));
    app.use(app.router);
    app.use(express.static(__dirname + '/public'));
});

app.configure('development', function () {
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function () {
    app.use(express.errorHandler());
});

// Routes

app.get('/', demo1.main);

app.listen(port);
console.log("Express server listening on port %d in %s mode", port, app.settings.env);
