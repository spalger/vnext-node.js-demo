var responses = [];
var async = require('async');

module.exports = {
    main: main
};

/**
MAIN ROUTE
*/

var main_php = 0;
var main_node = 0;

function main (req, res, next) {
    var listen = req.param('listen', null);
    var lang = req.param('lang', null);
    if (listen) {
        responses.push(res);
        res.on('end', function () {
            var i = responses.indexOf(res);
            if (~i) responses.splice(i, 1);
        });
    } else if (lang) {
        switch (lang) {
            case 'n':
                main_node += 1;
                main_respond(main_php, main_node);
                break;
            case 'p':
                main_php += 1;
                main_respond(main_php, main_node);
                break;
        }
        res.send('1');
    } else {
        debugger;
        res.send('please specify listen or language');
    }
};

function main_respond(php, node) {
    out = '---------------------------\n';
    out += ' PHP:' + php + ' NODE: ' + node + ' LISTENERS:'+responses.length+'\n';
    out += '---------------------------\n';
    async.forEach(
        responses,
        function (res, advance) {
            res.write(out);
            advance();
        }
    );
}